const express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    logger = require('morgan');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
app.use(require('./routes'));

app.listen(3000, function () {
    console.log('Listening on Port 3000');
});