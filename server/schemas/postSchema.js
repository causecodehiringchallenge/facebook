var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var commentSchema = require('./commentSchema.js');

var postSchema = new Schema({
    postId: String,
    postBody: {
        image: { data: Buffer, contentType: String },
        caption: String,
    },
    comments: [
        commentSchema
    ],
    likes: Number
});

exports.Schema(postSchema);