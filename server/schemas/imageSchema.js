var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var imageSchema = new Schema({
    data: Buffer,
    contentType: String
});

exports.Schema(imageSchema);