var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userSchema = require("./userSchema.js");

var dbSchema = new Schema({ users: [userSchema] });

exports.Schema(dbSchema);