var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var commentSchema = new Schema({
    userID: String,
    date: Date,
    body: String
});

exports.Schema(commentSchema);