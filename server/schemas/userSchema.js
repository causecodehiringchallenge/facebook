var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var profileSchema = require("./userSchema.js");
var postSchema = require("./postSchema.js");

var userSchema = new Schema({
    userID: String,
    profile: profileSchema,
    posts: [postSchema],
    Friends: [
        {
            userID: String
        }
    ]
});

exports.Schema(userSchema);