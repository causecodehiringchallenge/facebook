var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var commentSchema = require('./commentSchema.js');
var imageSchema = require('./imageSchema.js');

var profileSchema = new Schema({
    firstName: String,
    middleName: String,
    lastName: String,
    DOB: Date,
    profilePicture: imageSchema,
    photoCollection: [
        {
            image: imageSchema,
            likes: Number,
            comments: [
                commentSchema
            ]
        }
    ]
});

exports.Schema(profileSchema);