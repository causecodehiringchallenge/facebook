var express = require("express"),
    router = express.Router(),
    mongodb = require("mongodb"),
    errorhandler = require('errorhandler');

const url = "mongodb://localhost:27017/";

mongodb.MongoClient.connect(url, { useNewUrlParser: true }, (error, client) => {
    if (error) return process.exit(1);
    var db = client.db("facebook");

    router.get('/:userName', (req, res) => {
        db.collection('users')
            .find({ "userName": req.params.userName })
            .toArray((error, users) => {
                if (error) return next(error);
                res.send(users);
            });
    });

    router.get('/', (req, res) => {
        db.collection('users')
            .find()
            .toArray((error, users) => {
                if (error) return next(error);
                res.send(users);
            });
    });

    router.post('/', (req, res) => {
        console.log(req.body)
        let newUser = {
            userName: req.body.userName,
            profile: {
                firstName: req.body.profile.firstName,
                middleName: req.body.profile.middleName,
                lastName: req.body.profile.lastName,
                dob: req.body.profile.dob,
                profilePicture: "",
                photoCollection: []
            },
            posts: [],
            friends: []
        };
        db.collection('users').insert(newUser, (error, results) => {
            if (error) return next(error);
            res.send(results);
        });
    });

    router.put('/:userName', (req, res) => {
        let updatedUser = {
            userName: req.body.userName,
            profile: {
                firstName: req.body.profile.firstName,
                middleName: req.body.profile.middleName,
                lastName: req.body.profile.lastName,
                dob: req.body.profile.dob,
                profilePicture: req.body.profile.profilePicture,
                photoCollection: req.body.profile.photoCollection
            },
            posts: req.body.posts,
            friends: req.body.friends
        };
        db.collection('users')
            .update({ "userName": req.params.userName },
                { $set: updatedUser },
                (error, results) => {
                    if (error) return next(error);
                    res.send(results);
                });
    });

    router.delete('/:userName', (req, res) => {
        db.collection('users')
            .deleteOne({ "userName": req.params.userName }, (error, results) => {
                if (error) return next(error);
                res.send(results);
            });
    });
});

module.exports = router;